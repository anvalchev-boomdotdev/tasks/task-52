import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", (event) => {
    event.preventDefault();
  });

  let addArticle = () => {
    const article = document.createElement("article");
    article.classList.add("message");
    article.textContent = "Sample text";
    document.querySelector("body").appendChild(article);
  }

  document.body.addEventListener("click", (event) => {
    for (let i = 0; i < 5; i++) {
      addArticle();
    }
  });
});
